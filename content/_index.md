---
title: "Home"
image_paths:
    row1:
    - path: /img/Bee-Eater-on-wire-googly-eyes-768x768.png
      title: "Bee eater"
      description: "This is a bee-eater that I made."
      filename: "bee_eater"
      tags: ["bees"]
    - path: /img/Bee-hovering-1.png
      title: "Bee hovering"
      description: "Test description"
      tags: ["untagged"]
    - path: /img/Bluebell-stems.png
      title: "Bluebell stems"
      description: "Bluebells stems are nice"
      tags: ["untagged"]
    - path: /img/Blue-tit-with-googly-eyes.png
      title: "Blue tit"
      description: "Bluetit are nice"
      tags: ["untagged"]
    row2:
    - path: /img/Bug-portfolio-on-white-with-text-1086x1536.png
      title: "Bug portfolio"
      description: "Bugs"
      tags: ["untagged"]
    - path: /img/Bug-portfolio-colourful-SMALLER.png
      title: "Bug portfolio smaller"
      description: "Bug portfolio"
      tags: ["untagged"]
    - path: /img/scissors-small.png
      title: "Scissors"
      description: "Picture of scissors"
      tags: ["untagged"]
    row3:
    - path: /img/Butterflies-named-group-5-in-square-on-white-768x768.png
      title: "Butterflies named in group"
      description: "Picture of scissors"
      tags: ["untagged"]
    - path: /img/garden-birds-collection-named-on-sky-blue-768x768.png
      title: "Garden birds collection"
      description: "Picture of birdies"
      tags: ["untagged"]
    - path: /img/garden-birds-collection-named-on-white-768x768.png
      title: "Garden birds on white"
      description: "Bobbins "
      tags: ["untagged"]
    - path: /img/Spring-things-768x768.png
      title: "Spring things"
      description: "Lots of nice spring things"
      tags: ["untagged"]
    row4:
    - path: /img/Kingfisher-with-googly-eyes.png
      title: "Kingfisher"
      description: "Lots of nice spring things"
      tags: ["untagged"]
      filename: "kingfisher"
    - path: /img/Lemur-googly-white-eyes-on-lilac.png
      title: "Lemur with googly eyes"
      description: "Google eyes"
      tags: ["untagged"]
    - path: /img/Mod-Podge.png
      title: "Mod Podge glue"
      description: "A picture of mod podge glue"
      tags: ["untagged"]
    - path: /img/Peacock-butterfly-with-eyes-2.png
      title: "Peacok butterfly"
      description: "Butterfly with eyes"
      tags: ["untagged"]
---
