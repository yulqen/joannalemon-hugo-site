---
title: "About"
date: 2023-05-05
draft: false
---

I am a British illustrator from Yorkshire. I lived in London for twenty years and I’m now living in north Northumberland, very close to the Scottish Border with my little family in Berwick-upon-Tweed.

![Joanna Lemon](/img/jo_desk.jpg)

Inspired by nature I love to illustrate animals, plants, insects and birds - they're my happy place! My work is bright and detailed. I love vibrant colours, flat and textured and use tissue paper, collaged to build up layers for each image. I adore the work of Eric Carle and Clover Robin and I’m influenced by their perfectly imperfect finishes, celebration of colour and nature inspired details. 

![Joanna Lemon](/img/jo_desk2.jpg)

I graduated with a 2:1 BEd (with art) and taught primary and early years children for ten years. Through online courses I have been learning about children’s non fiction and picture book illustration over the last three years while building up my portfolio alongside writing and illustrating my own children’s books ready for submission. My work has been selected to advertise courses hosted by the Quentin Blake Centre for Illustration. 

![Joanna Lemon drawing](/img/jo_drawing.jpg)

I love all animals and have three cats who I both admire and go off (depending on whether they've brought home any ‘gifts’ recently). I hand sew various items both decorative nature inspired and practical pieces for children which I sell in my Etsy shop. I am dedicated to children’s illustration which I currently work on full time.

I am currently seeking representation and while I am sure I will flourish under guidance I am extremely responsive and tend to work like a machine when I am engaged in a project! 