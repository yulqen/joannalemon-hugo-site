---
title: "Rose in full bloom"
date: 2023-05-10
weight: 116
draft: false
tags: ["flower", "rose", "garden"]
image: "img/Rose full bloom.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "rose-in-full-bloom"
---

