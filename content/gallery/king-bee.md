---
title: "Bee King"
date: 2023-05-10
weight: 950
draft: false
tags: ["insect", "insects", "garden", "character"]
image: "img/Bee king.png"
image_alt: "Tissue paper collage. Created for the coronation of King Charles III."
description: "Tissue paper collage. Created for the coronation of King Charles III."
filename: "king-bee"
---

