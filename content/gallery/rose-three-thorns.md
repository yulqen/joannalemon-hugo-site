---
title: "Rose with three thorns"
date: 2023-05-09
weight: 65
draft: false
tags: ["flower", "pink", "garden"]
image: "img/Rose head thorns.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "rose-three-thorns"
---

