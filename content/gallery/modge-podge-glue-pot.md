---
title: "Mod Podge (tools of my trade)"
date: 2023-05-09
weight: 5
draft: false
tags: ["pink", "stationery", "tools"]
image: "img/Mod Podge.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "modge-podge-glue-pot"
---

