---
title: "Butterfly flutter"
date: 2023-05-09
weight: 55
draft: false
tags: ["butterfly", "butterflies", "garden"]
image: "img/Butterflies named group 5 in square on white.png"
image_alt: "Tissue paper collage. Peacock butterfly, Orange tip butterfly, Red Admiral butterfly, Green Malachite butterfly, Purple Emperor butterfly."
description: "Tissue paper collage. Peacock butterfly, Orange tip butterfly, Red Admiral butterfly, Green Malachite butterfly, Purple Emperor butterfly."
filename: "butterfly-flutter"
---

