---
title: "Scissors (tools of my trade)"
date: 2023-05-09
weight: 2500
draft: false
tags: ["blue", "yellow", "stationery", "tool"]
image: "img/scissors small.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "scissors-small"
---

