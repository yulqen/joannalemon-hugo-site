---
title: "Rose with bee"
date: 2023-05-10
weight: 146
draft: false
tags: ["flower", "rose", "garden", "insect", "insects"]
image: "img/Rose with bee.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "rose-with-bee"
---

