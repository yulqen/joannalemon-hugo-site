---
title: "Garden birds"
date: 2023-05-09
weight: 72
draft: false
tags: ["birds", "bird", "blue", "garden"]
image: "img/garden birds collection named on sky blue.png"
image_alt: "Tissue paper collage."
description: "Tissue paper collage."
filename: "garden-birds-sky"
---

