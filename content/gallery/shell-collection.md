---
title: "Shells"
date: 2023-05-09
weight: 74
draft: false
tags: ["natural-objects", "shells", "beach"]
image: "img/Shells collection.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "shell-collection"
---

