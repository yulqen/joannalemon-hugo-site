---
title: "Sparrow"
date: 2023-05-09
weight: 147
draft: false
tags: ["bird", "gardenbird"]
image: "img/Tree sparrow on branch final.png"
image_alt: "Tree sparrow. Tissue paper collage, April 2023"
description: "Tree sparrow. Tissue paper collage, April 2023"
filename: "sparrow"
---

