---
title: "Robin with paintbrush"
date: 2023-05-10
weight: 1
draft: false
tags: ["bird", "garden-bird"]
image: "img/Robin square with paintbrush.png"
image_alt: "Tissue paper collage. This is the sfirst bird I created using tissue paper collage, I wanted to see if it was possible to create a realistic bird in this way."
description: "Tissue paper collage. This is the sfirst bird I created using tissue paper collage, I wanted to see if it was possible to create a realistic bird in this way."
filename: "Robin-with-paintbrush"
---

