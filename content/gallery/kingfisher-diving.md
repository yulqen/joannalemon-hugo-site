---
title: "Kingfisher diving"
date: 2023-05-10
weight: 116
draft: false
tags: ["bird", "fish", "river", "prey"]
image: "img/Kingfisher diving for fish.png"
image_alt: "Tissue paper collage. I created this to be displayed on the virtual wall of Bologna Book Fair 2023."
description: "Tissue paper collage. I created this to be displayed on the virtual wall of Bologna Book Fair 2023."
filename: "kingfisher-diving"
---

