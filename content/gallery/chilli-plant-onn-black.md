---
title: "Chilli plant on BLACK"
date: 2023-05-10
weight: 76
draft: false
tags: ["plant", "food"]
image: "img/Plant Chilli plant on black.png"
image_alt: "Tissue paper collage. This was the first of two collages I made observing our chilli plant from life. There were three chillies on the plant at that point, two red and one, small green one."
description: "Tissue paper collage. This was the first of two collages I made observing our chilli plant from life. There were three chillies on the plant at that point, two red and one, small green one."
filename: "chilli-plant-on-black"
---

