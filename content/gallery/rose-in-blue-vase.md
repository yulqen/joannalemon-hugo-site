---
title: "Rose in blue vase"
date: 2023-05-09
weight: 140
draft: false
tags: ["flower", "rose", "garden", "orange"]
image: "img/Rose in blue vase.png"
image_alt: "Tissue paper collage"
description: "Tissue paper collage"
filename: "rose-in-blue-vase"
---

